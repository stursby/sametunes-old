var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});

io.on('connection', function (socket) {

    socket.on('clientWidgetStatus', function(data) {

        // console.log(data.status);
        // return;

        socket.broadcast.emit('serverWidgetStatus', { 'status': data.status });

    });

});

server.listen(3000);